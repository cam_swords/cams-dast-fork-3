import sys
import os
from custom_hooks import CustomHooks as CustomHooksClass
from zaproxy import ZAProxy as ZAProxyClass
from zap_webdriver import ZapWebdriver
from configuration import Configuration
import logging
from target_website import TargetWebsite
from system import System
from scan_script_wrapper import ScanScriptWrapper
from report import CombinedReportFormatter, CommonReportFormatter, ModifiedZapReportFormatter
from web_application_security_consortium import WebApplicationSecurityConsortium
import zap_full_scan_original
import zap_baseline_original

logging.basicConfig(level=logging.INFO, format='%(asctime)s %(message)s')

system = System()
config = Configuration.load(sys.argv[1:], os.environ)
zaproxy = ZAProxyClass(logging, config.target, config.auth_exclude_urls)
target_website = TargetWebsite(logging, config)
wasc = WebApplicationSecurityConsortium()
dast_report_formatter = CombinedReportFormatter(ModifiedZapReportFormatter(), CommonReportFormatter(wasc))

CustomHooks = CustomHooksClass(lambda: ZapWebdriver(config), zaproxy, dast_report_formatter, system)
FullScan = ScanScriptWrapper(zap_full_scan_original, config, logging, target_website, system)
BaselineScan = ScanScriptWrapper(zap_baseline_original, config, logging, target_website, system)
