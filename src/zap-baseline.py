#!/usr/bin/env python

# [DEPRECATED]
# Will be removed in the next major DAST release
# Refer to thread: https://gitlab.com/gitlab-org/security-products/dast/merge_requests/53#note_225806344

# This script wraps zap-baseline.py, which is coming with OWASP ZAP, for the purpose of supporting
# the syntax of GitLab DAST jobs as defined at
# https://docs.gitlab.com/ee/user/application_security/dast/index.html

from dependencies import BaselineScan

if __name__ == "__main__":
    BaselineScan.run()
