import types
from urlparse import urlparse


class ZAProxy:
    CONTEXT_NAME = 'Target Context'
    DEFAULT_CONTEXT_NAME = 'Default Context'

    def __init__(self, logging, target, auth_exclude_urls):
        self.zap = None
        self.target = None
        self.context_id = None
        self.logging = logging
        self.target = target
        self.auth_exclude_urls = auth_exclude_urls or ['(logout|signout)']

    def new_session(self, zap, target):
        # Hooks are used to integrate with the ZAProxy tool, so we don't have direct access to the underlying
        # zap (zapv2) variable. This is our first opportunity to use it, so we save it as an instance variable.
        self.zap = zap
        self.target = target

        # Unfortunately the zap_common.py code in the ZAProxy Docker image doesn't give us control over how
        # key APIs are called. We monkey patch them to ensure we can pass the options we want through.
        self.__monkey_patch_spider_scan()
        self.__monkey_patch_ajax_spider_scan()
        self.__monkey_patch_active_scan()

        # create a new session, with new contexts
        self.zap.core.new_session()
        self.context_id = self.create_context(self.CONTEXT_NAME)

        # include / exclude urls
        self.include_in_context('{uri.scheme}://{uri.netloc}.*'.format(uri=urlparse(target)))

        for url_regex in self.auth_exclude_urls:
            self.exclude_from_context(url_regex)

        if self.context_details(self.DEFAULT_CONTEXT_NAME):
            self.disable_context(self.DEFAULT_CONTEXT_NAME)

    def context_details(self, context_name):
        details = self.zap.context.context(context_name)

        if details == 'Does Not Exist':
            return None

        return details

    def create_context(self, context_name):
        self.logging.debug('Context - create ' + context_name)
        context_id = self.zap.context.new_context(self.CONTEXT_NAME)

        self.enable_context(self.CONTEXT_NAME)
        return context_id

    def include_in_context(self, url_regex):
        self.logging.debug('Context - include ' + url_regex)
        self.zap.context.include_in_context(self.CONTEXT_NAME, url_regex)

    def exclude_from_context(self, url_regex):
        self.logging.debug('Context - exclude ' + url_regex)
        self.zap.context.exclude_from_context(self.CONTEXT_NAME, url_regex)

    def enable_context(self, context_name):
        self.logging.debug('Context - enable ' + context_name)
        self.zap.context.set_context_in_scope(context_name, True)

    def disable_context(self, context_name):
        self.logging.debug('Context - disable ' + context_name)
        self.zap.context.set_context_in_scope(context_name, False)

    def __monkey_patch_spider_scan(self):
        if hasattr(self.zap.spider, 'original_scan'):
            return None

        def context_aware_scan(cls, target):
            self.logging.debug('Context - passive scan ' + self.CONTEXT_NAME)
            return cls.original_scan(target, contextname=self.CONTEXT_NAME)

        self.zap.spider.original_scan = self.zap.spider.scan
        self.zap.spider.scan = types.MethodType(context_aware_scan, self.zap.spider)

    def __monkey_patch_active_scan(self):
        if hasattr(self.zap.ascan, 'original_scan'):
            return None

        def context_aware_scan(cls, target, recurse=None, scanpolicyname=None):
            context_id = self.context_details(self.CONTEXT_NAME)['id']
            self.logging.debug('Context - active scan ' + context_id)
            return cls.original_scan(target, recurse=recurse, scanpolicyname=scanpolicyname, contextid=context_id)

        self.zap.ascan.original_scan = self.zap.ascan.scan
        self.zap.ascan.scan = types.MethodType(context_aware_scan, self.zap.ascan)

    def __monkey_patch_ajax_spider_scan(self):
        if hasattr(self.zap.ajaxSpider, 'original_scan'):
            return None

        def context_aware_scan(cls, target):
            self.logging.debug('Context - ajax scan ' + self.CONTEXT_NAME)
            return cls.original_scan(url=self.target, contextname=self.CONTEXT_NAME)

        self.zap.ajaxSpider.original_scan = self.zap.ajaxSpider.scan
        self.zap.ajaxSpider.scan = types.MethodType(context_aware_scan, self.zap.ajaxSpider)
