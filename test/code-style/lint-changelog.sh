#!/bin/bash

set -e

PROJECT_DIR="$(dirname "$(realpath "$0")")/../.."

# the parse script lints the changelog as it parses
"$PROJECT_DIR"/scripts/parse-changelog.sh >/dev/null
