#!/bin/bash
# Testing framework: https://github.com/pgrange/bash_unit

BUILT_IMAGE=${BUILT_IMAGE:-dast}

setup_suite() {
  mkdir -p output

  # install jq if not present
  command -v jq >/dev/null || apk add jq

  docker network create test >/dev/null

  docker run --rm \
    -v "${PWD}/fixtures/ajax-spider":/usr/share/nginx/html:ro \
    -v "${PWD}/fixtures/ajax-spider/nginx.conf":/etc/nginx/conf.d/default.conf \
    --name nginx \
    --network test \
    -d nginx >/dev/null
  true
}

teardown_suite() {
  docker rm --force nginx  >/dev/null 2>&1
  docker network rm test >/dev/null 2>&1
  true
}

test_ajax_spider_starts_at_target_url() {
  docker run --rm -v "${PWD}":/output \
    --network test \
    "${BUILT_IMAGE}" \
    /analyze -j -t http://nginx/food.html  >output/test_ajax_spider_starts_at_target_url.log 2>&1
  assert_equals "0" "$?" "Expected to exit without errors"

  jq . < gl-dast-report.json > output/report_test_ajax_spider_starts_at_target_url.json

  grep '"uri": "http://nginx/bacon.html"' gl-dast-report.json >/dev/null
  assert_equals "0" "$?" "expected vulnerabilities to be found for the dynamic bacon.html url, but there were none"
}

test_dynamic_urls_can_be_excluded_from_scan() {
  docker run --rm -v "${PWD}":/output \
    --network test \
    "${BUILT_IMAGE}" \
    /analyze -j -t http://nginx/food.html \
      --auth-exclude-urls 'http://nginx/bacon.html' \
     >output/test_dynamic_urls_can_be_excluded_from_scan.log 2>&1
  assert_equals "0" "$?" "Expected to exit without errors"

  jq . < gl-dast-report.json > output/report_test_dynamic_urls_can_be_excluded_from_scan.json

  grep '"uri": "http://nginx/bacon.html"' gl-dast-report.json >/dev/null
  assert_equals "1" "$?" "expected no vulnerabilities to be found for the dynamic bacon.html url, but there were some"
}
