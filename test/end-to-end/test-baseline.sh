#!/bin/bash
# Testing framework: https://github.com/pgrange/bash_unit

BUILT_IMAGE=${BUILT_IMAGE:-dast}

setup_suite() {
  mkdir -p output

  # install jq if not present
  command -v jq >/dev/null || apk add jq

  docker network create test >/dev/null

  docker run \
    --name nginx \
    -v "${PWD}/fixtures/basic-site":/usr/share/nginx/html:ro \
    -v "${PWD}/fixtures/basic-site/nginx.conf":/etc/nginx/conf.d/default.conf \
    -v "${PWD}/../unit/fixtures/certificates/self-signed.crt":/etc/nginx/self-signed.crt \
    -v "${PWD}/../unit/fixtures/certificates/self-signed.key":/etc/nginx/self-signed.key \
    --network test -d nginx >/dev/null

  true
}

teardown_suite() {
  docker rm --force nginx  >/dev/null 2>&1
  docker rm --force opt-param-test >/dev/null 2>&1
  docker network rm test >/dev/null 2>&1
  true
}

test_baseline_scan_using_t() {
  docker run --rm -v "${PWD}":/output --network test \
    "${BUILT_IMAGE}" /analyze -t http://nginx  >output/test_baseline_scan_using_t.log 2>&1
  assert_equals "0" "$?" "Expected to exit without errors"

  jq . < gl-dast-report.json > output/report_test_baseline_scan_using_t.json

  diff -u <(jq -S 'del(.["@generated"])' expect/test_baseline_scan_using_t.json) \
          <(jq -S 'del(.["@generated"])' gl-dast-report.json)
  assert_equals "0" "$?" "Analyze results differ from expectations"
}

test_baseline_scan_using_https_with_target_env_variable() {
  docker run --name 'opt-param-test' -v "${PWD}":/output --network test \
    -e DAST_WEBSITE=https://nginx "${BUILT_IMAGE}" /analyze -j -z very_optional_param >output/test_baseline_scan_using_https_with_target_env_variable.log 2>&1
  assert_equals "0" "$?" "Expected to exit without errors"

  jq . < gl-dast-report.json > output/report_test_baseline_scan_using_https_with_target_env_variable.json

  diff -u <(jq -S 'del(.["@generated"])' expect/test_baseline_scan_using_https_with_target_env_variable.json) \
          <(jq -S 'del(.["@generated"])' gl-dast-report.json)

  assert_equals "0" "$?" "Analyze results differ from expectations"

  docker logs 'opt-param-test' 2>&1 | grep 'Script params:' | grep "('-j', ''), ('-z', 'very_optional_param " >/dev/null
  assert_equals "0" "$?" "Optional parameter was not passed to ZAProxy"

  docker logs 'opt-param-test' 2>&1 | grep '\[zap.out\]' >/dev/null
  assert_equals "0" "$?" "ZAP logs should be part of logs"
}
