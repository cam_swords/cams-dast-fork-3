#!/bin/bash
# Testing framework: https://github.com/pgrange/bash_unit

BUILT_IMAGE=${BUILT_IMAGE:-dast}

setup_suite() {
  mkdir -p output

  # install jq if not present
  command -v jq >/dev/null || apk add jq

  docker network create test >/dev/null
  docker run --rm --name nginx -v "${PWD}/fixtures/basic-site":/usr/share/nginx/html:ro --network test -d nginx >/dev/null
  true
}

teardown_suite() {
  docker rm --force nginx  >/dev/null 2>&1
  docker network rm test >/dev/null 2>&1
  true
}

# Note that reports will always land in the /zap/wrk directory. Using an absolute path for report name does not work.
test_reports_can_be_exported() {
  docker run --rm -v "${PWD}":/output -v "${PWD}":/zap/wrk --network test \
    "${BUILT_IMAGE}" /analyze \
    -t http://nginx \
    -r report.html \
    -w report.md \
    -x report.xml \
    >output/test_reports_can_be_exported.log 2>&1
  assert_equals "0" "$?" "Expected to exit without errors"

  cp report.md output/report_test_reports_can_be_exported.md
  cp report.html output/report_test_reports_can_be_exported.html
  cp report.xml output/report_test_reports_can_be_exported.xml

  diff -u expect/test_reports_can_be_exported.html report.html
  assert_equals "0" "$?" "Html report differs from expectation"

  diff -u expect/test_reports_can_be_exported.md report.md
  assert_equals "0" "$?" "Markdown report differs from expectation"

  # remove the generated timestamp from the created xml report
  # note sed -i is not used as it does not work consistently on different operating systems
  sed 's/generated=".*"/generated="__REMOVED__"/g' report.xml > temp.xml && mv temp.xml report.xml

  diff -u --ignore-all-space expect/test_reports_can_be_exported.xml report.xml
  assert_equals "0" "$?" "Xml report differs from expectation"
}

test_aborts_when_invalid_target_url_supplied() {
  docker run --rm \
    "${BUILT_IMAGE}" /analyze -t not-a-url \
    >output/test_aborts_when_invalid_target_url_supplied.log 2>&1
  assert_equals "2" "$?" "Expected to exit with a non zero exit code"

  grep "argument -t: not-a-url is not a valid URL" output/test_aborts_when_invalid_target_url_supplied.log >/dev/null
  assert_equals "0" "$?" "Should print message to friendly error message to user"
}

test_aborts_when_target_url_is_not_supplied() {
  docker run --rm \
    "${BUILT_IMAGE}" /analyze -t "" \
    >output/test_aborts_when_target_url_is_not_supplied.log 2>&1
  assert_equals "2" "$?" "Expected to exit with a non zero exit code"
}

test_help_documentation() {
  docker run --rm \
    "${BUILT_IMAGE}" /analyze --help \
     >output/test_help_documentation.log 2>&1
  assert_equals "0" "$?" "Help documentation was not outputed, test failed."
}
