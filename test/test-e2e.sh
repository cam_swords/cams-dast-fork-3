#!/bin/bash
set -e

TEST=${1:-"test-*.sh"}

find ./test/end-to-end -name "$TEST" -print0 | xargs -0 bash_unit
