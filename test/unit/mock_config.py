class ToConfig:
    def __init__(self, **key_values):
        self.__dict__.update(key_values)

    def __getattr__(self, method_name):
        if method_name in self.__dict__:
            return self.__dict__.get(method_name)
        else:
            return None
