import unittest
import json
from unittest.mock import MagicMock
from src.report import CombinedReportFormatter
from collections import OrderedDict


class ToObject:
    def __init__(self, **key_values):
        self.__dict__.update(key_values)


class CombinedReportFormatterTest(unittest.TestCase):

    def test_should_combine_output_from_formatters(self):
        formatter_a = ToObject(format=MagicMock(return_value=OrderedDict({'animal': 'platypus'})))
        formatter_b = ToObject(format=MagicMock(return_value=OrderedDict({'reptile': 'crocodile'})))

        report = CombinedReportFormatter(formatter_a, formatter_b).format('report', 'spider', 'spider_result')
        self.assertEqual(json.dumps(report), '{"animal": "platypus", "reptile": "crocodile"}')

    def test_should_combine_output_from_formatters_in_alphabetical_order(self):
        formatter_a = ToObject(format=MagicMock(return_value=OrderedDict({'a': 1})))
        formatter_b = ToObject(format=MagicMock(return_value=OrderedDict({'c': 1})))
        formatter_c = ToObject(format=MagicMock(return_value=OrderedDict({'b': 1})))

        report = CombinedReportFormatter(formatter_a, formatter_b, formatter_c).format('rpt', 'spdr', 'spdr_result')
        self.assertEqual(json.dumps(report), '{"a": 1, "b": 1, "c": 1}')

    def test_should_pass_parameters_to_formatters(self):
        formatter_a = ToObject(format=MagicMock(return_value=OrderedDict()))
        formatter_b = ToObject(format=MagicMock(return_value=OrderedDict()))

        CombinedReportFormatter(formatter_a, formatter_b).format('zap_report', 'zap_spider', 'zap_spider_result')

        formatter_a.format.assert_called_with('zap_report', 'zap_spider', 'zap_spider_result')
        formatter_b.format.assert_called_with('zap_report', 'zap_spider', 'zap_spider_result')

    def test_should_barf_when_formatters_return_json_keys_with_same_name(self):
        formatter_a = ToObject(format=MagicMock(return_value=OrderedDict({'value': 1})))
        formatter_b = ToObject(format=MagicMock(return_value=OrderedDict({'value': 2})))

        try:
            CombinedReportFormatter(formatter_a, formatter_b).format('report', 'spider', 'spider_result')
            self.assertTrue(False)
        except RuntimeError as e:
            self.assertEqual(str(e), "Cannot create combined report as multiple formatters produce property 'value'")
