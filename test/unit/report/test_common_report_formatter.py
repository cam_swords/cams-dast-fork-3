import unittest
from unittest.mock import MagicMock
import json
from src.report import CommonReportFormatter


class ToObject:
    def __init__(self, **key_values):
        self.__dict__.update(key_values)


def zap_report():
    return {
        '@generated': 'Tue, 22 Oct 2019 01:01:55',
        '@version': 'D-2019-09-23',
        'site': [
            {
                '@port': '80',
                '@host': 'nginx',
                '@name': 'http://nginx',
                'alerts': [
                    {
                        'count': '1',
                        'riskdesc': 'Low (Medium)',
                        'name': 'Web Browser XSS Protection Not Enabled',
                        'reference': '<p>http://blogs.msdn.com/a</p><p>http://blogs.msdn.com/b</p>',
                        'otherinfo': '<p>The X-XSS-Protection HTTP response header allows the web server ...</p>',
                        'sourceid': '3',
                        'confidence': '2',
                        'alert': 'Web Browser XSS Protection Not Enabled',
                        'instances': [
                            {
                                'attack': 'http://nginx/attack',
                                'evidence': '<form action="/myform" method="POST">',
                                'uri': 'http://nginx/',
                                'param': 'X-XSS-Protection',
                                'method': 'POST'
                            }
                        ],
                        'pluginid': '10016',
                        'riskcode': '1',
                        'wascid': '14',
                        'solution': '<p>Ensure that the web browsers XSS filter is enabled...</p>',
                        'cweid': '933',
                        'desc': '<p>Web Browser XSS Protection is not enabled, or is disabled...</p>'
                    },
                    {
                        'count': '2',
                        'riskdesc': 'Medium (Medium)',
                        'name': 'X-Frame-Options Header Not Set',
                        'reference': '<p>http://blogs.msdn.com/b/ieinternals/archive/2010/03/30/...</p>',
                        'sourceid': '3',
                        'confidence': '2',
                        'alert': 'X-Frame-Options Header Not Set',
                        'instances': [
                            {
                                'uri': 'http://nginx/b.location',
                                'param': 'X-Frame-Options',
                                'method': 'GET'
                            },
                            {
                                'uri': 'http://nginx/a.location',
                                'param': 'X-Frame-Options',
                                'method': 'GET'
                            }
                        ],
                        'pluginid': '10020',
                        'riskcode': '2',
                        'wascid': '15',
                        'solution': '<p>Most modern Web browsers support the X-Frame-Options HTTP header...</p>',
                        'cweid': '16',
                        'desc': '<p>X-Frame-Options header is not included in the HTTP response...</p>'
                    }
                ],
                '@ssl': 'false'
            }
        ]
    }


class CommonReportFormatterTest(unittest.TestCase):

    def setUp(self):
        self.wasc = ToObject(reference_url_for_id=MagicMock(return_value='http://projects.webappsec.org/reference'))
        self.report = CommonReportFormatter(self.wasc).format(zap_report())

    def test_should_contain_a_version(self):
        self.assertTrue(self.report['version'])

    def test_vulnerabilities_include_all_properties(self):
        self.assertEqual(len(self.report['vulnerabilities']), 3)

        vulnerability = self.report['vulnerabilities'][2]
        self.assertEqual(vulnerability['category'], 'dast')
        self.assertEqual(vulnerability['confidence'], 'medium')
        self.assertEqual(vulnerability['cve'], '10016')
        self.assertEqual(vulnerability['description'], 'Web Browser XSS Protection is not enabled, or is disabled...')
        self.assertEqual(len(vulnerability['identifiers']), 3)
        self.assertEqual(vulnerability['identifiers'][0]['name'], 'Web Browser XSS Protection Not Enabled')
        self.assertEqual(vulnerability['identifiers'][0]['type'], 'ZAProxy_PluginId')
        self.assertIn('https://github.com/zaproxy/zaproxy/', vulnerability['identifiers'][0]['url'])
        self.assertEqual(vulnerability['identifiers'][0]['value'], '10016')
        self.assertEqual(vulnerability['identifiers'][1]['name'], 'CWE-933')
        self.assertEqual(vulnerability['identifiers'][1]['type'], 'CWE')
        self.assertEqual(vulnerability['identifiers'][1]['url'], 'https://cwe.mitre.org/data/definitions/933.html')
        self.assertEqual(vulnerability['identifiers'][1]['value'], '933')
        self.assertEqual(vulnerability['identifiers'][2]['name'], 'WASC-14')
        self.assertEqual(vulnerability['identifiers'][2]['type'], 'WASC')
        self.assertEqual(vulnerability['identifiers'][2]['url'], 'http://projects.webappsec.org/reference')
        self.assertEqual(vulnerability['identifiers'][2]['value'], '14')
        self.assertEqual(len(vulnerability['links']), 2)
        self.assertEqual(vulnerability['links'][0]['url'], 'http://blogs.msdn.com/a')
        self.assertEqual(vulnerability['links'][1]['url'], 'http://blogs.msdn.com/b')
        self.assertEqual(vulnerability['location']['param'], 'X-XSS-Protection')
        self.assertEqual(vulnerability['location']['method'], 'POST')
        self.assertEqual(vulnerability['location']['hostname'], 'http://nginx')
        self.assertEqual(vulnerability['location']['path'], '/')
        self.assertEqual(vulnerability['message'], 'Web Browser XSS Protection Not Enabled')
        self.assertEqual(vulnerability['scanner']['id'], 'zaproxy')
        self.assertEqual(vulnerability['scanner']['name'], 'ZAProxy')
        self.assertEqual(vulnerability['severity'], 'low')
        self.assertEqual(vulnerability['solution'], 'Ensure that the web browsers XSS filter is enabled...')

    def test_vulnerabilities_are_ordered_by_name_and_risk(self):
        self.assertEqual(len(self.report['vulnerabilities']), 3)

        self.assertEqual(self.report['vulnerabilities'][0]['message'], 'X-Frame-Options Header Not Set')
        self.assertIn('/a.location', self.report['vulnerabilities'][0]['location']['path'])
        self.assertEqual(self.report['vulnerabilities'][1]['message'], 'X-Frame-Options Header Not Set')
        self.assertIn('/b.location', self.report['vulnerabilities'][1]['location']['path'])
        self.assertEqual(self.report['vulnerabilities'][2]['message'], 'Web Browser XSS Protection Not Enabled')
        self.assertIn('/', self.report['vulnerabilities'][2]['location']['path'])

    def test_vulnerability_severity_is_mapped_to_human_readable_form(self):
        def build_report_with_zap_riskcode(riskcode):
            zap = zap_report()

            for alert in zap['site'][0]['alerts']:
                alert['name'] = 'generic.name'
                alert['riskcode'] = '-1'

            zap['site'][0]['alerts'][0]['riskcode'] = riskcode
            return CommonReportFormatter(self.wasc).format(zap)

        report = build_report_with_zap_riskcode('0')
        self.assertEqual(report['vulnerabilities'][0]['severity'], 'info')

        report = build_report_with_zap_riskcode('1')
        self.assertEqual(report['vulnerabilities'][0]['severity'], 'low')

        report = build_report_with_zap_riskcode('2')
        self.assertEqual(report['vulnerabilities'][0]['severity'], 'medium')

        report = build_report_with_zap_riskcode('3')
        self.assertEqual(report['vulnerabilities'][0]['severity'], 'high')

        report = build_report_with_zap_riskcode('666')
        self.assertEqual(report['vulnerabilities'][0]['severity'], 'unknown')

    def test_vulnerability_confidence_is_mapped_to_human_readable_form(self):
        def build_report_with_zap_confidence(confidence):
            zap = zap_report()

            for alert in zap['site'][0]['alerts']:
                alert['name'] = 'generic.name'
                alert['riskcode'] = '-1'

            zap['site'][0]['alerts'][0]['riskcode'] = '1'
            zap['site'][0]['alerts'][0]['confidence'] = confidence
            return CommonReportFormatter(self.wasc).format(zap)

        report = build_report_with_zap_confidence('0')
        self.assertEqual(report['vulnerabilities'][0]['confidence'], 'ignore')

        report = build_report_with_zap_confidence('1')
        self.assertEqual(report['vulnerabilities'][0]['confidence'], 'low')

        report = build_report_with_zap_confidence('2')
        self.assertEqual(report['vulnerabilities'][0]['confidence'], 'medium')

        report = build_report_with_zap_confidence('3')
        self.assertEqual(report['vulnerabilities'][0]['confidence'], 'high')

        report = build_report_with_zap_confidence('4')
        self.assertEqual(report['vulnerabilities'][0]['confidence'], 'confirmed')

        report = build_report_with_zap_confidence('666')
        self.assertEqual(report['vulnerabilities'][0]['confidence'], 'unknown')

    def test_vulnerability_location_contains_uri(self):
        def build_report_with_zap_uri(uri):
            zap = zap_report()

            for alert in zap['site'][0]['alerts']:
                alert['name'] = 'generic.name'
                alert['riskcode'] = '-1'

            zap['site'][0]['alerts'][0]['riskcode'] = '1'
            zap['site'][0]['alerts'][0]['instances'][0]['uri'] = uri
            return CommonReportFormatter(self.wasc).format(zap)

        report = build_report_with_zap_uri('https://fred:flintstone@domain.com/some/path?search=foo#fragment')
        self.assertEqual(report['vulnerabilities'][0]['location']['hostname'], 'https://fred:flintstone@domain.com')
        self.assertEqual(report['vulnerabilities'][0]['location']['path'], '/some/path?search=foo#fragment')

        report = build_report_with_zap_uri('https://website.com/')
        self.assertEqual(report['vulnerabilities'][0]['location']['hostname'], 'https://website.com')
        self.assertEqual(report['vulnerabilities'][0]['location']['path'], '/')

        report = build_report_with_zap_uri('https://website.com')
        self.assertEqual(report['vulnerabilities'][0]['location']['hostname'], 'https://website.com')
        self.assertEqual(report['vulnerabilities'][0]['location']['path'], '')

    def test_should_not_have_cwe_identifier_when_not_present(self):
        def build_report_with_zap_cwe_id(cwe_id):
            zap = zap_report()

            for alert in zap['site'][0]['alerts']:
                alert['name'] = 'generic.name'
                alert['riskcode'] = '-1'

            zap['site'][0]['alerts'][0]['riskcode'] = '1'
            zap['site'][0]['alerts'][0]['cweid'] = cwe_id
            return CommonReportFormatter(self.wasc).format(zap)

        report = build_report_with_zap_cwe_id('')
        types = [identifier['type'] for identifier in report['vulnerabilities'][0]['identifiers']]
        self.assertFalse('CWE' in types)

    def test_should_not_have_wasc_identifier_when_not_present(self):
        def build_report_with_zap_wasc_id(wasc_id):
            zap = zap_report()

            for alert in zap['site'][0]['alerts']:
                alert['name'] = 'generic.name'
                alert['riskcode'] = '-1'

            zap['site'][0]['alerts'][0]['riskcode'] = '1'
            zap['site'][0]['alerts'][0]['wascid'] = wasc_id
            return CommonReportFormatter(self.wasc).format(zap)

        report = build_report_with_zap_wasc_id('')
        types = [identifier['type'] for identifier in report['vulnerabilities'][0]['identifiers']]
        self.assertFalse('WASC' in types)

    def test_should_barf_when_vulnerability_has_no_pluginid(self):
        zap_rpt = zap_report()
        zap_rpt['site'][0]['alerts'][0]['pluginid'] = ''

        try:
            CommonReportFormatter(self.wasc).format(zap_rpt)
            self.assertTrue(False)
        except RuntimeError as e:
            self.assertEqual(str(e), "Unable to create DAST report as there is no "
                                     "pluginid for alert 'Web Browser XSS Protection Not Enabled'")
