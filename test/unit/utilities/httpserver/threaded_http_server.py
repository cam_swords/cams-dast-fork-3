from threading import Thread
from http.server import HTTPServer
from .get_free_port import get_free_port
import ssl


class ThreadedHTTPServer:
    def __init__(self, handler_class, https_certificate_path):
        self.port = get_free_port()
        self.handler_class = handler_class
        self.server = None
        self.server_thread = None
        self.https_certificate_path = https_certificate_path

    def address(self):
        protocol = 'https' if self.https_certificate_path else 'http'
        return '{}://127.0.0.1:{}'.format(protocol, self.port)

    def start(self):
        self.server = HTTPServer(('127.0.0.1', self.port), self.handler_class)

        if self.https_certificate_path:
            self.server.socket = ssl.wrap_socket(self.server.socket,
                                                 certfile='{}.crt'.format(self.https_certificate_path),
                                                 keyfile='{}.key'.format(self.https_certificate_path),
                                                 server_side=True)

        self.server_thread = Thread(target=self.server.serve_forever)
        self.server_thread.daemon = True
        self.server_thread.start()

    def stop(self):
        if self.server:
            self.server.shutdown()
            self.server.server_close()
            self.server = self.server_thread = None
